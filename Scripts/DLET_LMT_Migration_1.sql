use database product_analytics_prod_db;
use schema dlet_lmt;
select current_database(), current_schema();

-- DLET_LMT.SC2_798_loafing_automated definition

CREATE or replace TABLE SC2_798_loafing_automated
     (
      Serial_Number VARCHAR(20),
      Duration FLOAT,
      Event_Name VARCHAR(20),
      Event_Start_DateTime TIMESTAMP,
      Event_Stop_DateTime TIMESTAMP,
      Min_Payload_tonnes FLOAT,
      Max_Payload_tonnes FLOAT,
      Min_Body_Angle FLOAT,
      Max_Body_Angle FLOAT,
      Body_Acceleration FLOAT,
      Avg_StrutBR FLOAT,
      Avg_StrutBL FLOAT,
      Avg_StrutFR FLOAT,
      Avg_StrutFL FLOAT,
      Min_Hst_HE_P FLOAT,
      Max_Hst_HE_P FLOAT,
      Min_Hst_RE_P FLOAT,
      Max_Hst_RE_P FLOAT,
      Latitude FLOAT,
      Longitude FLOAT,
      MaxThrottle FLOAT,
      MinThrottle FLOAT,
      MaxENGSPD FLOAT,
      MinENGSPD FLOAT);
 
DESC TABLE SC2_798_LOAFING_AUTOMATED;
-- DLET_LMT.Trolley_795_performance definition

CREATE OR REPLACE TABLE Trolley_795_performance
     (
      Serial_Number VARCHAR(20),
      SMU_Hours_Min FLOAT,
      Duration FLOAT,
      Event_Name VARCHAR(20),
      Event_Start_DateTime TIMESTAMP,
      Event_Stop_DateTime TIMESTAMP,
      Min_Payload_tonnes FLOAT,
      Max_Payload_tonnes FLOAT,
      Min_GndSpd_kph FLOAT,
      Max_GndSpd_kph FLOAT,
      Avg_GndSpd_kph FLOAT,
      GPS_Latitude FLOAT,
      GPS_Longitude FLOAT,
      TrolleyPower_Min FLOAT,
      TrolleyPower_Max FLOAT,
      TrolleyPower_Avg FLOAT,
      EngSpd_Min FLOAT,
      EngSpd_Max FLOAT,
      EngSpd_Avg FLOAT,
      Fuel_Rate_LPH_Avg FLOAT,
      Load_Factor_Avg FLOAT,
      Eng_Power_Actual_kw_Avg FLOAT);

-- DLET_LMT.Trolley_795_eventcounts definition

CREATE OR REPLACE TABLE Trolley_795_eventcounts (
      Serial_Number VARCHAR(20),
      Duration FLOAT,
      Event_Name VARCHAR(20),
      Event_Start_DateTime TIMESTAMP(6),
      Event_Stop_DateTime TIMESTAMP(6));
      
CREATE or replace TABLE SC2_798T4_ST7_TorqueTemps (
      Serial_Number VARCHAR(20),
      Duration FLOAT,
      Event_Name VARCHAR(20),
      Event_Start_DateTime TIMESTAMP,
      Event_Stop_DateTime TIMESTAMP,
      Max_Payload_tonnes FLOAT,
      Min_GndSpd_kph FLOAT,
      Max_GndSpd_kph FLOAT,
      Peak_MotorTorqueLeft_Nm FLOAT,
      Peak_MotorTorqueRight_Nm FLOAT,
      Max_InverterPower_kW FLOAT,
      GPS_Latitude FLOAT,
      GPS_Longitude FLOAT,
      Travel_Direction VARCHAR(20),
      Max_MtrR_Bear1_Temp FLOAT,
      Max_MtrR_Bear2_Temp FLOAT,
      Max_MtrR_Wind1_Temp FLOAT,
      Max_MtrR_Wind2_Temp FLOAT,
      Max_MtrL_Bear1_Temp FLOAT,
      Max_MtrL_Bear2_Temp FLOAT,
      Max_MtrL_Wind1_Temp FLOAT,
      Max_MtrL_Wind2_Temp FLOAT,
      Max_Ambient_Temp FLOAT,
      Max_Throttle FLOAT);      
      
-- DLET_LMT.SC2_798T4_ST7_HoistCycle_Segments definition

CREATE or replace TABLE SC2_798T4_ST7_HoistCycle_Segments (
      Serial_Number VARCHAR(20),
      Duration FLOAT,
      Event_Start_DateTime TIMESTAMP(6),
      Event_Stop_DateTime TIMESTAMP(6),
      Dump_Type VARCHAR(20),
      Avg_EngSpd FLOAT,
      Avg_Payload FLOAT,
      Avg_FuelRate FLOAT,
      Avg_GndSpd FLOAT,
      Avg_BodyAngle FLOAT);

-- DLET_LMT.SC2_798T4_ST7_HoistCycle_Types definition

CREATE or replace TABLE SC2_798T4_ST7_HoistCycle_Types (
      Serial_Number VARCHAR(20),
      Duration FLOAT,
      Event_Start_DateTime TIMESTAMP(6),
      Event_Stop_DateTime TIMESTAMP(6),
      Dump_Type VARCHAR(20),
      Avg_EngSpd FLOAT,
      Avg_Payload FLOAT,
      Avg_FuelRate FLOAT,
      Avg_GndSpd FLOAT,
      Avg_BodyAngle FLOAT);
      
CREATE OR REPLACE TABLE SC2_798T4_ST7_TractionControl (
      Serial_Number VARCHAR(20),
      Duration FLOAT,
      Event_Name VARCHAR(20),
      Event_Start_DateTime TIMESTAMP(6),
      Event_Stop_DateTime TIMESTAMP(6),
      Max_Payload_tonnes FLOAT,
      Min_GndSpd_kph FLOAT,
      Max_GndSpd_kph FLOAT,
      Peak_MotorTorqueLeft_Nm FLOAT,
      Peak_MotorTorqueRight_Nm FLOAT,
      Max_InverterPower_kW FLOAT,
      GPS_Latitude FLOAT,
      GPS_Longitude FLOAT,
      Max_Ambient_Temp FLOAT,
      Max_Throttle FLOAT);     
      
CREATE OR REPLACE TABLE SC2_Strain_Gauge_HRZ_ST7_automated (
      serial_number VARCHAR(20),
      "date" TIMESTAMP(6),
      SHM INTEGER,
      param_name VARCHAR(20),
      true_life FLOAT);    
      
DESC TABLE SC2_Strain_Gauge_HRZ_ST7_automated; 

CREATE OR REPLACE TABLE SC2_T4_PerCycle_DEF_automated (
      serial_number VARCHAR(20),
      start_timestamp TIMESTAMP(6),
      end_timestamp TIMESTAMP(6),
      "total_cycle_time(min)" FLOAT,
      max_payload_weight_tonnes FLOAT,
      load_factor FLOAT,
      "Fuel_consumed_per_cycle(gal)" FLOAT,
      "DEF_consumed_per_cycle(liters)" FLOAT,
      "DEF_consumed_per_cycle(gal)" FLOAT,
      "Fuel_rate_per_cycle(gal/hr)" FLOAT,
      "DEF_rate_per_cycle(lit/hr)" FLOAT,
      "DEF_rate_per_cycle(gal/hr)" FLOAT,
      "%DEF/Fuel_consump" FLOAT,
      "total_SpecFuelConsump_per_cycle(gal)" FLOAT,
      "distance_per_cycle(km)" FLOAT,
      dump_latitude FLOAT,
      dump_longitude FLOAT,
      load_latitude FLOAT,
      load_longitude FLOAT);
      
CREATE or replace TABLE SC2_798_MotorTemps (
      SER_NUM VARCHAR(20),
      GLBL_TM_TS TIMESTAMP(3),
      MACHINE_SPEED_KPH NUMBER,
      PAYLOAD_TONNES NUMBER,
      MOTOR_R_TORQUE_NM NUMBER,
      MOTOR_L_TORQUE_NM NUMBER,
      MOTOR_L_POWER_KW NUMBER,
      MOTOR_L_SPEED NUMBER,
      MOTOR_R_POWER_KW NUMBER,
      MOTOR_R_SPEEED NUMBER,
      MOTOR_L_BEARING1_TEMP NUMBER,
      MOTOR_L_BEARING2_TEMP NUMBER,
      MOTOR_L_WINDING1_TEMP NUMBER,
      MOTOR_L_WINDING2_TEMP NUMBER,
      ALTERNATOR_F_BEARING_TEMP NUMBER,
      ALTERNATOR_R_BEARING_TEMP NUMBER,
      ALTERNATOR_F_STATOR_TEMP NUMBER,
      ALTERNATOR_R_STATOR_TEMP NUMBER,
      MOTOR_R_BEARING1_TEMP NUMBER,
      MOTOR_R_BEARING2_TEMP NUMBER,
      MOTOR_R_WINDING1_TEMP NUMBER,
      MOTOR_R_WINDING2_TEMP NUMBER,
      AMBIENT_TEMP_C NUMBER,
      GPRMC_LATITUDE NUMBER,
      GPRMC_LONGITUDE NUMBER);   
      
-- DLET_LMT.SC2_794_MONITORING definition

CREATE OR REPLACE TABLE SC2_794_MONITORING (
      SER_NUM VARCHAR(20),
      YR INTEGER,
      MTH INTEGER,
      DY INTEGER,
      HR INTEGER,
      FlsysOut_LoadFactor FLOAT,
      press_boost FLOAT,
      press_boost_2 FLOAT,
      press_oil_abs FLOAT,
      press_oil_gauge FLOAT,
      temp_fuel FLOAT,
      temp_fuel_rail FLOAT,
      FuelFiltSSDiffPress FLOAT,
      temp_turbine_inlet_l FLOAT,
      temp_turbine_inlet_r FLOAT,
      MOTOR_L_WINDING1_TEMP FLOAT,
      MOTOR_L_WINDING2_TEMP FLOAT,
      MOTOR_R_WINDING1_TEMP FLOAT,
      MOTOR_R_WINDING2_TEMP FLOAT,
      BRAKE_BR_T FLOAT,
      BRAKE_BL_T FLOAT,
      BRAKE_FR_T FLOAT,
      BRAKE_FL_T FLOAT,
      seconds DECIMAL(15,0),
      hrs DECIMAL(16,0));      
      
DESC TABLE SC2_794_MONITORING;   

-- DLET_LMT.SC2_794_MotorTemps definition

CREATE OR REPLACE TABLE SC2_794_MotorTemps (
      SER_NUM VARCHAR(20),
      GLBL_TM_TS TIMESTAMP(3),
      MACHINE_SPEED_KPH NUMBER,
      PAYLOAD_TONNES NUMBER,
      MOTOR_R_TORQUE_NM NUMBER,
      MOTOR_L_TORQUE_NM NUMBER,
      MOTOR_L_SPEED NUMBER,
      MOTOR_R_SPEEED NUMBER,
      MOTOR_L_BEARING2_TEMP NUMBER,
      MOTOR_L_WINDING1_TEMP NUMBER,
      MOTOR_R_BEARING2_TEMP NUMBER,
      MOTOR_R_WINDING1_TEMP NUMBER,
      AMBIENT_TEMP_C NUMBER);
      
CREATE OR REPLACE TABLE SC2_794T4_TECK_ROADS (
      SER_NUM VARCHAR(20),
      GLBL_TM_TS TIMESTAMP(3),
      ENGINE_SPEED_RPM NUMBER,
      MACHINE_SPEED_KPH NUMBER,
      PAYLOAD_TONNES NUMBER,
      BODY_ANGLE_DEG NUMBER,
      STRUT_BL_KPA NUMBER,
      STRUT_BR_KPA NUMBER,
      STRUT_FL_KPA NUMBER,
      STRUT_FR_KPA NUMBER,
      WHEEL_FL_RPM NUMBER,
      WHEEL_FR_RPM NUMBER,
      STEER_ANGLE_EST NUMBER,
      STEER_CYL_L_KPA NUMBER,
      STEER_CYL_R_KPA NUMBER,
      LAT_ACC_MPS2 NUMBER,
      LON_ACC_MPS2 NUMBER,
      VERT_ACC_MPS2 NUMBER,
      PITCH_ANGLE_DEG NUMBER,
      PITCH_RATE_DPS NUMBER,
      ROLL_ANGLE_DEG NUMBER,
      ROLL_RATE_DPS NUMBER,
      YAW_RATE_DPS NUMBER,
      GPRMC_LATITUDE NUMBER,
      GPRMC_LONGITUDE NUMBER,
      GPRMC_ALTITUDE NUMBER,
      P2 NUMBER,
      A1 NUMBER,
      A2 NUMBER);      