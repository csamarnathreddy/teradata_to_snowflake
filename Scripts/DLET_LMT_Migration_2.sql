use database product_analytics_prod_db;
use schema dlet_lmt;

--Create Table RAC_eventcount_HRT00101
CREATE OR REPLACE TABLE RAC_eventcount_HRT00101 (
      Serial_Number VARCHAR(20),
      Event_Name VARCHAR(20),
      Event_Start_DateTime TIMESTAMP(6),
      Event_Stop_DateTime TIMESTAMP(6),
      Duration FLOAT);

CREATE OR REPLACE TABLE RAC_eventcount_HRZ00501 (
      Serial_Number VARCHAR(20),
      Event_Name VARCHAR(20),
      Event_Start_DateTime TIMESTAMP(6),
      Event_Stop_DateTime TIMESTAMP(6),
      Duration FLOAT);

CREATE OR REPLACE TABLE RAC_eventcount_ST700400 (
    Serial_Number VARCHAR(20),
      Event_Name VARCHAR(20),
      Event_Start_DateTime TIMESTAMP(6),
      Event_Stop_DateTime TIMESTAMP(6),
      Duration FLOAT);
      
CREATE OR REPLACE TABLE VIMS_FanRPM (
      SerNum VARCHAR(20),
      GlblTmTs TIMESTAMP(3),
      FAN_SPEED_RPM NUMBER);

CREATE OR REPLACE TABLE VIMS_KUCC_ARC (
      SerNum VARCHAR(20),
      GlblTmTs TIMESTAMP(3),
      RETARD_POS NUMBER,
      RETARD_MODE NUMBER);

CREATE OR REPLACE TABLE VIMS_794_christmascreek (
      SerNum VARCHAR(20),
      GlblTmTs TIMESTAMP(3),
      MACHINE_SPEED_KPH NUMBER,
      STRUT_BL_KPA NUMBER,
      STRUT_BR_KPA NUMBER,
      STRUT_FL_KPA NUMBER,
      STRUT_FR_KPA NUMBER,
      FRONT_DIFF_R_L_KPA NUMBER,
      REAR_DIFF_R_L_KPA NUMBER,
      PAYLOAD_TONNES NUMBER);

PUT 'file://C:\\Users\\reddya3\\Desktop\\Teradata\\VIMS_793F_MT_WHALEBACK_CSV.csv' @teradata_dlet_lmt_stage auto_compress=true;

select $1, $4, $4::number, $4::number(25,3) from @teradata_dlet_lmt_stage/VIMS_793F_MT_WHALEBACK_CSV.csv limit 25;

CREATE OR REPLACE TABLE VIMS_793F_MT_WHALEBACK  (
      SERIAL_NUMBER VARCHAR(20),
      GLOBAL_TS TIMESTAMP(3),
      MACH_SPD FLOAT,
      PAYLOAD FLOAT,
      GEAR NUMBER,
      PRNDL NUMBER,
      STRUT_BL_KPA NUMBER,
      STRUT_BR_KPA NUMBER,
      STRUT_FL_KPA NUMBER,
      STRUT_FR_KPA NUMBER,
      LR_SPD NUMBER,
      RR_SPD NUMBER);
      
COPY into VIMS_793F_MT_WHALEBACK from @teradata_dlet_lmt_stage/VIMS_793F_MT_WHALEBACK_CSV.csv size_limit=25;
select * from VIMS_793F_MT_WHALEBACK;

CREATE OR REPLACE TABLE SC2_794_RAC_events_automated (
      Serial_Number VARCHAR(20),
      Event_Name VARCHAR(20),
      Event_Start_DateTime TIMESTAMP(6),
      Event_Stop_DateTime TIMESTAMP(6),
      Duration FLOAT);

CREATE OR REPLACE TABLE SC2_794_RAC_events_automated_v2 (
      Serial_Number VARCHAR(20),
      Event_Name VARCHAR(20),
      Event_Start_DateTime TIMESTAMP(6),
      Event_Stop_DateTime TIMESTAMP(6),
      Duration FLOAT);
      
CREATE OR REPLACE TABLE SC2_798_RAC_events_automated (
      Serial_Number VARCHAR(20),
      Event_Name VARCHAR(20),
      Event_Start_DateTime TIMESTAMP(6),
      Event_Stop_DateTime TIMESTAMP(6),
      Duration FLOAT);

CREATE OR REPLACE TABLE SC2_798_RAC_events_automated_v2 (
      Serial_Number VARCHAR(20),
      Event_Name VARCHAR(20),
      Event_Start_DateTime TIMESTAMP(6),
      Event_Stop_DateTime TIMESTAMP(6),
      Duration FLOAT);

PUT 'file://C:\\Users\\reddya3\\Desktop\\Teradata\\Table Exports for Snowflake\\SC2_798_RAC_eventframe_v5_CSV.csv' @teradata_dlet_lmt_stage auto_compress=true;
select $1, $2, $3, $4::number(25,10) from @teradata_dlet_lmt_stage/SC2_798_RAC_eventframe_v5_CSV.csv limit 25;

CREATE OR REPLACE TABLE SC2_798_RAC_eventframe_v5 (
      segment_id VARCHAR(50),
      time FLOAT,
      GLOBAL_TIME FLOAT,
      Strut_BL_P_A5_UR FLOAT,
      Strut_BR_P_A5_UR FLOAT,
      Strut_FL_P_A5_UR FLOAT,
      Strut_FR_P_A5_UR FLOAT,
      Pitch FLOAT,
      Bias FLOAT,
      Rack FLOAT,
      Event VARCHAR(20));
      
SELECT * FROM SC2_798_RAC_eventframe_v5;
COPY INTO SC2_798_RAC_eventframe_v5 FROM @teradata_dlet_lmt_stage/SC2_798_RAC_eventframe_v5_CSV.csv;  

PUT 'file://C:\\Users\\reddya3\\Desktop\\Teradata\\Table Exports for Snowflake\\SC2_798_RAC_eventframe_alldata_CSV.csv' @teradata_dlet_lmt_stage auto_compress=true;
select $1, $2, $3, $4 from @teradata_dlet_lmt_stage/SC2_798_RAC_eventframe_alldata_CSV.csv limit 25;
select count($1) from @teradata_dlet_lmt_stage/SC2_798_RAC_eventframe_alldata_CSV.csv;
select * from SC2_798_RAC_eventframe_alldata;
--Analyze data on row 21, col 2

CREATE table SC2_798_RAC_eventframe_alldata as 
select
      $1::VARCHAR(50) AS SEGMENT_ID ,
      $2::FLOAT AS TIME,
      $3::FLOAT AS GLOBAL_TIME,
      $4::FLOAT AS STRUT_BL_P_A5_UR,
      $5::FLOAT AS STRUT_BR_P_A5_UR,
      $6::FLOAT AS STRUT_FL_P_A5_UR,
      $7::FLOAT AS STRUT_FR_P_A5_UR,
      $8::FLOAT AS PITCH,
      $9::FLOAT AS BIAS,
      $10::FLOAT AS RACK,
      $11::VARCHAR(20) AS Event
from @teradata_dlet_lmt_stage/SC2_798_RAC_eventframe_alldata_CSV.csv;

snowsql -a cat_ohio.us-east-2.aws -u Reddy_Amarnath@cat.com --authenticator externalbrowser
PUT 'file://C:\\Users\\reddya3\\Desktop\\Teradata\\Table Exports for Snowflake\\SC2_798_RAC_eventframe_alldata_25v2_CSV.csv' @teradata_dlet_lmt_stage auto_compress=true;
PUT 'file://C:\\Users\\reddya3\\Desktop\\Teradata\\Table Exports for Snowflake\\SC2_798_RAC_eventframe_alldata_25v3_CSV.csv' @teradata_dlet_lmt_stage auto_compress=true;


CREATE TABLE SC2_798_RAC_EVENTFRAME_ALLDATA_25V2 AS
select
      $1::VARCHAR(50) AS SEGMENT_ID ,
      $2::FLOAT AS TIME,
      $3::FLOAT AS GLOBAL_TIME,
      $4::FLOAT AS STRUT_BL_P_A5_UR,
      $5::FLOAT AS STRUT_BR_P_A5_UR,
      $6::FLOAT AS STRUT_FL_P_A5_UR,
      $7::FLOAT AS STRUT_FR_P_A5_UR,
      $8::FLOAT AS PITCH,
      $9::FLOAT AS BIAS,
      $10::FLOAT AS RACK,
      $11::VARCHAR(20) AS Event
from @teradata_dlet_lmt_stage/SC2_798_RAC_eventframe_alldata_25v2_CSV.csv;

SELECT * FROM SC2_798_RAC_EVENTFRAME_ALLDATA_25V2;

CREATE TABLE SC2_798_RAC_EVENTFRAME_ALLDATA_25V3 AS
select
      $1::VARCHAR(50) AS SEGMENT_ID ,
      $2::FLOAT AS TIME,
      $3::FLOAT AS GLOBAL_TIME,
      $4::FLOAT AS STRUT_BL_P_A5_UR,
      $5::FLOAT AS STRUT_BR_P_A5_UR,
      $6::FLOAT AS STRUT_FL_P_A5_UR,
      $7::FLOAT AS STRUT_FR_P_A5_UR,
      $8::FLOAT AS PITCH,
      $9::FLOAT AS BIAS,
      $10::FLOAT AS RACK,
      $11::FLOAT AS PAYLOAD_STATE,
      $12::VARCHAR(20) AS Event
from @teradata_dlet_lmt_stage/SC2_798_RAC_eventframe_alldata_25v3_CSV.csv;

SELECT * FROM SC2_798_RAC_EVENTFRAME_ALLDATA_25V3;
CREATE OR REPLACE TABLE SC2_798_RAC_eventframe_alldata_25v4 (
      SEGMENT_ID VARCHAR(50),
      TIME FLOAT,
      GLOBAL_TIME FLOAT,
      STRUT_BL_P_A5_UR FLOAT,
      STRUT_BR_P_A5_UR FLOAT,
      STRUT_FL_P_A5_UR FLOAT,
      STRUT_FR_P_A5_UR FLOAT,
      PITCH FLOAT,
      BIAS FLOAT,
      RACK FLOAT,
      PAYLOAD_STATE FLOAT,
      EVENT VARCHAR(20));     
SELECT * FROM SC2_798_RAC_EVENTFRAME_ALLDATA_25V4;

COPY INTO SC2_798_RAC_EVENTFRAME_ALLDATA_25V4 FROM @TERADATA_DLET_LMT_STAGE/SC2_798_RAC_eventframe_alldata_25v4_CSV.csv;
 
  CREATE OR REPLACE TABLE USM_DATA_IMPORT (
      Serial_Number VARCHAR(8),
      Crack BYTEINT,
      USM_Status BYTEINT,
      Hours VARCHAR(6),
      MaxStrutLF VARCHAR(6),
      MaxStrutRF VARCHAR(6),
      AveStrutLF VARCHAR(5),
      AveStrutRF VARCHAR(5),
      AvePitchFELA VARCHAR(5),
      AveRackFELA VARCHAR(5),
      MaxPitchFELA VARCHAR(5),
      MaxRackFELA VARCHAR(5)
  );

describe table USM_data_import;

show stages;
list @TERADATA_DLET_LMT_TSV_STAGE;
list @teradata_dlet_lmt_stage;

snowsql -a cat_ohio.us-east-2.aws -u Reddy_Amarnath@cat.com --authenticator externalbrowser
use database product_analytics_prod_db;
use schema dlet_lmt;
PUT 'file://C:\\Users\\reddya3\\Desktop\\Teradata\\Table Exports for Snowflake\\SC2_T4_PerCycle_DEF_automated_CSV.csv' @teradata_dlet_lmt_stage auto_compress=true;
PUT 'file://C:\\Users\\reddya3\\Desktop\\Teradata\\Table Exports for Snowflake\\USM_data_import_TSV.csv' @TERADATA_DLET_LMT_TSV_STAGE auto_compress=true;

select $1, $2, $3, $4, $5, $6 from @TERADATA_DLET_LMT_TSV_STAGE/USM_data_import_TSV.csv;

copy into USM_DATA_IMPORT FROM @TERADATA_DLET_LMT_TSV_STAGE/USM_data_import_TSV.csv file_format = 'TERADATA_PSV_FF';
select * from USM_DATA_IMPORT;
delete from USM_DATA_IMPORT;

CREATE OR REPLACE TABLE SC2_T4_PERCYCLE_DEF_AUTOMATED (
      SERIAL_NUMBER VARCHAR(20),
      START_TIMESTAMP TIMESTAMP(6),
      END_TIMESTAMP TIMESTAMP(6),
      TOTAL_CYCLE_TIME_MIN FLOAT,
      MAX_PAYLOAD_WEIGHT_TONNES FLOAT,
      LOAD_FACTOR FLOAT,
      FUEL_CONSUMED_PER_CYCLE_GAL FLOAT,
      DEF_CONSUMED_PER_CYCLE_LITERS FLOAT,
      DEF_CONSUMED_PER_CYCLE_GAL FLOAT,
      FUEL_RATE_PER_CYCLE_GAL_HR FLOAT,
      DEF_RATE_PER_CYCLE_LIT_HR FLOAT,
      DEF_RATE_PER_CYCLE_GAL_HR FLOAT,
      PERCENTAGE_DEF_FUEL_CONSUMP FLOAT,
      TOTAL_SPEC_FUEL_CONSUMP_PER_CYCLE_GAL FLOAT,
      DISTANCE_PER_CYCLE_KM FLOAT,
      DUMP_LATITUDE FLOAT,
      DUMP_LONGITUDE FLOAT,
      LOAD_LATITUDE FLOAT,
      LOAD_LONGITUDE FLOAT);

select $1, $2, $3, $4, $15, $17 from @teradata_dlet_lmt_stage/SC2_T4_PerCycle_DEF_automated_CSV.csv;
select count($1) from @teradata_dlet_lmt_stage/SC2_T4_PerCycle_DEF_automated_CSV.csv;


select * from SC2_T4_PERCYCLE_DEF_AUTOMATED;

copy into SC2_T4_PERCYCLE_DEF_AUTOMATED from @teradata_dlet_lmt_stage/SC2_T4_PerCycle_DEF_automated_CSV.csv;